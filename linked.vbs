Monitor production applications, evaluate the severity and business impact.

Utilizing the following software:

- MongoDB
- IBM Platform Symphony - EGO
- Apache Zookeeper
- Zabbix
- Elasticsearch
- CA Autosys
- GIT / Stashblue / JIRA / Perforce
- Twiki
- IBM MQ
- ServiceNow Integration with Incidents, Requests, iAnnounce, iAlert
Collaborate proactive and reactively according to the requirements of the client, providing shell or perl scripts.
